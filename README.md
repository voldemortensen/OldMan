# OldMan

Make the Old Man from The Legend of Zelda say anything you want.

Contributions welcome! Please create issues or make pull requests! By contributing you agree to the Code of Conduct for this project.

## Dependencies

Until I figure out how to include a font in this, you'll need to install the [VT323](https://fonts.google.com/specimen/VT323) font.

## Usage

To build the executable:

```
git clone <repo>
cd <repo>
swift build
```

The executable can then be run:

```
./.build/debug/OldMan "Hello, World!"
```

The executable can be moved wherever you'd like.

## Known and Potential Issues

This only works on OS X for now. I'd love to write a cross-platform solution.

This was tested on a 15 inch screen. Other screens will probably have weird layout issues with text. Swift is a new language for me and I haven't figured out layouts yet.

## Thanks

The base code for scaling fonts came from @joncardasis and can be found [in his gist](https://gist.github.com/joncardasis/4d40e9782ca6b1f3d2ef8abd0290794c). I modified the code to work for NSText.

## Privacy Note

Until I figure out a way to embed an image in the executable, this pulls a GIF from my personal website. I only keep basic server logs and they're regularly rotated. If you're a privacy minded person and care about that sort of thing, this is your heads up.
