import Cocoa
import AppKit
import Foundation

var crappyGlobal = false

@available(OSX 10.12, *)
final public class OldMan: NSObject, NSApplicationDelegate {
    var mainWindow: NSWindow?

    @objc public func applicationDidFinishLaunching(_ notification: Notification) {
        let window = OldManWindow(
            contentRect: (NSScreen.main?.frame)!,
            styleMask: [.titled, .closable, .fullScreen, .borderless, .fullSizeContentView, .resizable],
            backing: .buffered,
            defer: true
        )

        window.backgroundColor = NSColor.black
        window.title = "OldMan"
        window.center()
        window.maxFullScreenContentSize = (NSScreen.main?.frame.size)!
        window.minFullScreenContentSize = (NSScreen.main?.frame.size)!
        window.orderFrontRegardless()

        let logo = getLogo()
        let logoView = NSImageView(frame: window.frame)
        logoView.animates = true
        logoView.image = logo
        logoView.imageScaling = NSImageScaling.scaleProportionallyUpOrDown

        window.contentView?.addSubview(logoView)

        let msg = NSText(frame: NSRect(x: 350, y: 700, width: 990, height: 100))
        msg.maxSize = CGSize(width: 990, height: 100)
        msg.drawsBackground = false
        msg.string = ""
        msg.isSelectable = false
        msg.isEditable = false
        msg.alignment = NSTextAlignment.center

        var message = "Please supply a message.\n./OldMan <message>"
        if CommandLine.arguments.count > 1 {
            message = CommandLine.arguments[1]
        }

        msg.font = NSFont(name: "VT323", size: 144)
        msg.string = message
        msg.fitTextToBounds()
        msg.string = ""
        msg.typeOn(string: message)

        window.contentView?.addSubview(msg)

        window.toggleFullScreen(_: self)
        self.mainWindow = window

        NSApplication.shared.activate(ignoringOtherApps: true)
    }

    @objc func quit(_: Any) {
        NSApplication.shared.terminate(self)
    }

    func getLogo() -> NSImage {
        let url = URL(string: "https://www.garthmortensen.com/yikess.gif")
        let req = URLRequest(url: (url)!)
        let data = self.getStuff(req: req)

        return NSImage(data: data)!
    }

    func getStuff(req: URLRequest) -> Data {
        let session = URLSession.shared
        var dataReceived: Data = Data ()
        let sem = DispatchSemaphore(value: 0)

        let task = session.dataTask(with: req) { data, _, error in
            if error != nil {
                return
            }

            dataReceived = data!
            _ = sem.signal()
        }

        task.resume()

        _ = sem.wait(timeout: DispatchTime.distantFuture)
        return dataReceived
    }

    @objc public func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }
}
