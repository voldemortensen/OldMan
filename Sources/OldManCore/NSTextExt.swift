import AppKit

@available(OSX 10.12, *)
extension NSText {
    /// Will auto resize the contained text to a font size which fits the frames bounds.
    /// Uses the pre-set font to dynamically determine the proper sizing
    func fitTextToBounds() {
        let text = self.string
        let currentFont = self.font!

        let bestFittingFont = NSFont.bestFittingFont(
            for: text,
            in: bounds,
            fontDescriptor: currentFont.fontDescriptor,
            additionalAttributes: basicStringAttributes)
        font = bestFittingFont
    }

    private var basicStringAttributes: [NSAttributedString.Key: Any] {
        var attribs = [NSAttributedString.Key: Any]()

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = self.alignment
        attribs[.paragraphStyle] = paragraphStyle

        return attribs
    }

    func typeOn(string: String) {
        var charArr = Array(string)
        var characterIndex = 0

        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true) { (timer) in
            if charArr[characterIndex] != "$" {
                while charArr[characterIndex] == " " {
                    self.string.append(" ")
                    characterIndex += 1
                    if characterIndex == charArr.count {
                        timer.invalidate()
                        crappyGlobal = true
                        return
                    }
                }
                self.string.append(charArr[characterIndex])
            }
            characterIndex += 1
            if characterIndex == charArr.count {
                timer.invalidate()
                crappyGlobal = true
            }
        }
    }
}
