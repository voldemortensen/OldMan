import OldManCore
import AppKit

let mainApp = NSApplication.shared
mainApp.setActivationPolicy(.regular)

if #available(OSX 10.12.2, *) {
    let appController = OldMan()
    mainApp.delegate = appController

    mainApp.run()
} else {
    print("App could not be run.")
}
